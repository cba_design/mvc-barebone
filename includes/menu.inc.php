<?php
require_once("class.permission.php");
$authorization = new Permission();
?>
<div class="ch-container">
    <div class="row">
	
	        <noscript>
            <div class="alert alert-block col-md-12">
                <h4 class="alert-heading">Waarschuwing!</h4>

                <p>U heeft  <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a>
                    Om deze website te kunnen gebruiken.</p>
            </div>
        </noscript>

        
        <!-- left menu starts -->
        <div class="col-sm-2 col-lg-2">
            <div class="sidebar-nav">
                <div class="nav-canvas">
                    <div class="nav-sm nav nav-stacked">

                    </div>
                    <ul class="nav nav-pills nav-stacked main-menu">
                        <li class="nav-header">Hoofdmenu</li>
                        <li class="accordion">
                            <a href="#"><i class="glyphicon glyphicon-plus"></i><span> Acties</span></a>
                            <ul class="nav nav-pills nav-stacked">
                                <li><a href="/action/show/true">Volbracht</a></li>
                                <li><a href="/action/show/false">Openstaand</a></li>
                            </ul>
                        </li>
                        <li><a class="ajax-link" href="/company"><i class="glyphicon glyphicon-eye-open"></i><span> Bedrijven</span></a>
                        </li>
						<?php if($authorization->ShowMenuItem("Manage") === true): ?>
						<li class="accordion">
                            <a href="#"><i class="glyphicon glyphicon-file"></i><span> Contracten</span></a>
                            <ul class="nav nav-pills nav-stacked">
								<li><a class="ajax-link" href="/candidate"><span> Kandidaten</span></a></li>
								<li><a class="ajax-link" href="/article"><span> Artikelen</span></a></li>
								<li><a class="ajax-link" href="/contract"></i><span> Contract opstellen</span></a></li>
                            </ul>
                        </li>
						<li class="accordion">
                            <a href="#"><i class="glyphicon glyphicon-wrench"></i><span> Beheer</span></a>
                            <ul class="nav nav-pills nav-stacked">
								<li><a class="ajax-link" href="/function"><span> Functie beheer</span></a></li>
								<li><a class="ajax-link" href="/branche"><span> Branche beheer</span></a></li>
								<li><a class="ajax-link" href="/manage"></i><span> Gebruikers beheer</span></a></li>
                            </ul>
                        </li>
							<li><a class="ajax-link" href="/template"><i class="glyphicon glyphicon-list-alt"></i><span> Template inhoud</span></a></li>
						<li class="accordion">
                            <a href="#"><i class="glyphicon glyphicon-envelope"></i><span> Mail</span></a>
                            <ul class="nav nav-pills nav-stacked">
                                <li><a href="/mail">Versturen</a></li>
                                <li><a href="/mail/history">Geschiedenis</a></li>
                            </ul>
                        </li>
							<li><a class="ajax-link" href="/export"><i class="glyphicon glyphicon-list-alt"></i><span> Exports</span></a></li>
						<?php endif; ?>
                    </ul>
                </div>
            </div>
        </div>
        <!--/span-->
        <!-- left menu ends -->