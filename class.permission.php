<?php

class Permission extends Model{
		
	public function IsAuthorized($attribute){
		session_start();
		$sql = "SELECT Permission FROM users WHERE UserId=:id";
		
		$this->_setSql($sql);
		$this->_setParam(array(":id" => $_SESSION['userId']));
		
		$row = $this->getRow($sql);
		
		foreach($row as $value){
			$role = $row["Permission"];
		}
	
		$permission = explode(":", $role);

		foreach($permission as $value){
			if($value == $attribute){
				return true;
			}
		}
		
		$_SESSION['active'] = "";
		header('location:/login');
		die();
	}
	
	public function ShowMenuItem($attribute){
		session_start();
		$sql = "SELECT Permission FROM users WHERE UserId=:id";
		
		$this->_setSql($sql);
		$this->_setParam(array(":id" => $_SESSION['userId']));
		
		$row = $this->getRow($sql);
		
		foreach($row as $value){
			$role = $row["Permission"];
		}
	
		$permission = explode(":", $role);

		foreach($permission as $value){
			if($value == $attribute){
				return true;
			}
		}
		return false;
	}
}