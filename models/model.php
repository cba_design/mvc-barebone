<?php


class Model{
	
	protected $_db;
	protected $_sql;
	protected $_sqlParam;
	
	public function __construct(){
		$this->_db = Db::init();
	}
	
	protected function _setSql($sql){
		$this->_sql = $sql;
	}

	protected function _setParam($params){
		$this->_sqlParam = $params;
	}
	
	public function getAll($data = null){
		
		if (!$this->_sql){
			throw new Exception("No SQL query!");
		}
		
		$sth = $this->_db->prepare($this->_sql);
		
		foreach ($this->_sqlParam as $key => $value) {
			$sth->bindValue($key,$value);
		}
		
		$sth->execute();
		return $sth->fetchAll();
	}
	
	public function getRow($data = null){
		
		if (!$this->_sql){
			throw new Exception("No SQL query!");
		}
		
		$sth = $this->_db->prepare($this->_sql);
		
		foreach ($this->_sqlParam as $key => $value) {
		$sth->bindValue($key,$value);
		}
		$sth->execute();
		return $sth->fetch();
	}
	
	public function editRow($data = null){
		
		if (!$this->_sql){
			throw new Exception("No SQL query!");
		}
		
		$sth = $this->_db->prepare($this->_sql);
		
		foreach ($this->_sqlParam as $key => $value) {
		    $sth->bindValue($key,$value);
		}
		
		$sth->execute();
		return $sth->rowCount();
	}
	
	

	
}