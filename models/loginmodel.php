<?php

class LoginModel extends Model{
	
	public function LoginUser($username,$password){
		
		$sql = "SELECT * FROM users WHERE UserName = :username AND Active = 'true'";
		
		$this->_setSql($sql);
		
		$this->_setParam(array(":username" => $username));
		$loginUser = $this->getRow($sql);
				
		if($this->ValidatePassword($loginUser['Password'], $password) === true){

			session_start();
			
			//Change the session id on login
            session_regenerate_id(true);
			
			$_SESSION['access'] = "active";
			$_SESSION['userId'] = $loginUser['UserId'];
			$_SESSION['userName'] = $loginUser['UserName'];
			$_SESSION['csrf'] = base64_encode(openssl_random_pseudo_bytes(128));
			return $loginUser;
   		}
		
		return false;
	}
	
	//Takes a password and returns the salted hash
	//returns - the hash of the password
	public function HashPassword($password){
		$options = [
    		'cost' => 11,
					];
		
	$hash =	password_hash($password, PASSWORD_BCRYPT, $options)."\n";
	
	return $hash;
	}
	
	//Validates a password
	//returns true if match correct
	public function ValidatePassword($correctHash, $password){
		if (password_verify($password, $correctHash)) {
			return true;
		}
	}
}

