<?php
class DocumentModel extends Model{
	
	public function SetDocumentId($documentId){$this->_documentId = $documentId;}
	
	public function SetDocumentName($documentName){$this->_documentName = $documentName;}
	
	public function SetDescription($description){$this->_description = $description;}
	
	public function SetCompanyId($companyId){$this->_companyId= $companyId;}
			
	public function GetDocumentsById($documentId){
		
		$sql = " SELECT  * FROM documents WHERE DocumentId=:id";
	
		$this->_setSql($sql);
		$this->_setParam(array(":id" => $documentId));

		$documents = $this->getAll();
		
		if (empty($documents)){
			return false;	
		}
		return $documents; 				
	} 
	
	public function GetDocumentsByCompanyId($companyId){
		
		$sql = " SELECT  * FROM documents WHERE CompanyId=:id";
	
		$this->_setSql($sql);
		$this->_setParam(array(":id" => $companyId));

		$documents = $this->getAll();
		
		if (empty($documents)){
			return false;	
		}
		return $documents; 				
	} 
	
	public function StoreDocument(){
		
			session_start();
			
			$sql = "INSERT INTO documents
						(CompanyId, Name, Description)
					VALUES 
						(?,?,?)";
			
			$data = array(
				$this->_companyId,
				$this->_documentName,
				$this->_description
			);
			
			$sth = $this->_db->prepare($sql);
			return $sth->execute($data);
	}		
	
	public function UpdateDocument(){
		
			session_start();
			
			$sql = "UPDATE documents SET Description=? WHERE DocumentId=?";
			
			$data = array(
				$this->_description,
				$this->_documentId
			);

			$sth = $this->_db->prepare($sql);
			return $sth->execute($data);
	} 
		
	
	public function RemoveDocument(){
		
		$sql = "DELETE FROM documents WHERE DocumentId=:id";
	
		$this->_setSql($sql);
		$this->_setParam(array(":id" => $this->_documentId));

		$delete = $this->editRow($sql);

		if (empty($delete)){
			return false;	
		}
		return $delete; 				
	} 	
}
?>