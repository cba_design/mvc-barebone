<?php
class CandidateModel extends Model{

	public function SetCandidateId($candidateId){$this->_candidateId = $candidateId;}
	
	public function SetCandidateName($candidateName){$this->_candidateName = $candidateName;}
	
	public function SetCandidateLastName($candidateLastName){$this->_candidateLastName = $candidateLastName;}
	
	public function SetCandidateDOB($candidateDOB){$this->_candidateDOB = $candidateDOB;}
	
	public function SetCandidateZipCode($candidateZipCode){$this->_candidateZipCode = $candidateZipCode;}
	
	public function SetCandidateCity($candidateCity){$this->_candidateCity = $candidateCity;}
	
	public function SetCandidateAddress($candidateAddress){$this->_candidateAddress = $candidateAddress;}
	
	public function SetCandidateGender($candidateGender){$this->_candidateGender = $candidateGender;}
	
	public function SetCandidateHours($candidateHours){$this->_candidateHours = $candidateHours;}
	
	public function SetCandidateDays($candidateDays){$this->_candidateDays = $candidateDays;}
	
	public function SetCandidateType($candidateType){$this->_candidateType = $candidateType;}
	
	public function SetCandidateStartDate($candidateStartDate){$this->_candidateStartDate = $candidateStartDate;}
	
	public function SetCandidateEndDate($candidateEndDate){$this->_candidateEndDate = $candidateEndDate;}
	
	public function SetCandidateSalary($candidateSalary){$this->_candidateSalary = $candidateSalary;}
		
		
    public function Getcandidates(){
		
        $sql = "SELEcT * FROM candidates";
		
        $this->_setSql($sql);
        $candidates = $this->getAll();
         
        if (empty($candidates)){
            return false;
        }
        return $candidates;
    }
	
	public function GetCandidatesById($candidateId){
		
		$sql = " SELEcT  * FROM candidates WHERE CandidateId=:id";
	
		$this->_setSql($sql);
		$this->_setParam(array(":id" => $candidateId));

		$getByID = $this->getAll();
		
		if (empty($getByID)){
			return false;	
		}
		return $getByID; 				
	} 
	
	public function StoreCandidate(){
		
			session_start();
			
			$sql = "INSERT INTO candidates
						(Name, LastName, DOB, ZipCode, City, Address, Gender, Type, Hours, Days, StartDate, EndDate, Salary)
					VALUES 
						(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
			
			$data = array(
				$this->_candidateName,
				$this->_candidateLastName,
				$this->_candidateDOB,
				$this->_candidateZipCode,
				$this->_candidateCity,
				$this->_candidateAddress,
				$this->_candidateGender,
				$this->_candidateType,
				$this->_candidateHours,
				$this->_candidateDays,
				$this->_candidateStartDate,
				$this->_candidateEndDate,
				$this->_candidateSalary
			);
			
			$sth = $this->_db->prepare($sql);
			return $sth->execute($data);
	}		
	
	public function UpdateCandidate(){
		
			session_start();
			
			$sql = "UPDATE candidates SET Name=?, LastName=?, DOB=?, ZipCode=?, City=?, Address=?, Gender=?, Type=?, Hours=?, Days=?, StartDate=?, EndDate=?, Salary=? WHERE CandidateId=?";
			
			$data = array(
				$this->_candidateName,
				$this->_candidateLastName,
				$this->_candidateDOB,
				$this->_candidateZipCode,
				$this->_candidateCity,
				$this->_candidateAddress,
				$this->_candidateGender,
				$this->_candidateType,
				$this->_candidateHours,
				$this->_candidateDays,
				$this->_candidateStartDate,
				$this->_candidateEndDate,
				$this->_candidateSalary,
				$this->_candidateId
			);
			
			$sth = $this->_db->prepare($sql);
			return $sth->execute($data);
	} 
	
	public function RemoveCandidate(){
		
		$sql = "DELETE FROM candidates where CandidateId=:id";
	
		$this->_setSql($sql);
		$this->_setParam(array(":id" => $this->_candidateId));

		$delete = $this->editRow($sql);

		if (empty($delete)){
			return false;	
		}
		return $delete; 				
	} 	
}
?>