<?php

require_once("class.permission.php");

class Candidatecontroller extends Controller{
		
    public function __construct($model, $action){
        parent::__construct($model, $action);
        $this->_setModel($model);
    }
    
    public function index($completed){
        try {
			
			// Authentication check        	         	     
            $this->_checkSession();
			
			$authorization = new Permission();
			$authorization->IsAuthorized("Manage");
			
            $candidates  = $this->_model->GetCandidates();
			$this->_view->set('candidates', $candidates);
			
            $this->_view->set('title', 'Beheer applicatie:: Kandidaten');
             
            return $this->_view->output();
             
        } catch (Exception $e) {
            echo "Application error:" . $e->getMessage();
        }
    }
	     
	public function Edit($update){
		
		//Authentication check        	         	     
        $this->_checkSession();
			
		$authorization = new Permission();
		$authorization->IsAuthorized("Manage");
		
		//check cSRF
		$this->_checkcsrf($_POST['CSRF']);
		
		$name 	   = isset($_POST['candidateName']) ? trim($_POST['candidateName']) : NULL;
		$lastName  = isset($_POST['lastName']) 	    ? trim($_POST['lastName']) : NULL;
		$city 	   = isset($_POST['city']) 	        ? trim($_POST['city']) : NULL;
		$zipCode   = isset($_POST['zipCode'])  	    ? trim($_POST['zipCode']) : NULL;
		$address   = isset($_POST['address'])  	    ? trim($_POST['address']) : NULL;
		$DOB 	   = isset($_POST['DOB']) 	  	    ? trim($_POST['DOB']) : NULL;
		$gender    = isset($_POST['gender'])   	    ? trim($_POST['gender']) : NULL;
		$type 	   = isset($_POST['type'])    		? trim($_POST['type']) : NULL;
		$hours 	   = isset($_POST['hours'])    		? trim($_POST['hours']) : NULL;
		$days      = isset($_POST['days'])  	  	? trim($_POST['days']) : NULL;
		$salary    = isset($_POST['salary'])    	? trim($_POST['salary']) : NULL;
		$startDate = isset($_POST['startDate'])     ? trim($_POST['startDate']) : NULL;
		$endDate   = isset($_POST['endDate'])    	? trim($_POST['endDate']) : NULL;
		$candidateId   = isset($_POST['candidateId'])    	? trim($_POST['candidateId']) : NULL;

		try {
					
			$candidate = new CandidateModel();
			$candidate->SetCandidateName($name);
			$candidate->SetCandidateLastName($lastName);
			$candidate->SetCandidateCity($city);
			$candidate->SetCandidateZipCode($zipCode);
			$candidate->SetCandidateAddress($address);
			$candidate->SetCandidateDOB($DOB);
			$candidate->SetCandidateGender($gender);
			$candidate->SetCandidateId($candidateId);
			$candidate->SetCandidateType($type);
			$candidate->SetCandidateHours($hours);
			$candidate->SetCandidateDays($days);
			$candidate->SetCandidateSalary($salary);
			$candidate->SetCandidateStartDate($startDate);
			$candidate->SetCandidateEndDate($endDate);
			
			if($update == ""){
			    $candidate->StoreCandidate();
			}
			if($update == "update"){
				$candidate->UpdateCandidate();
			}
			
			header('location:/candidate');
			
		} catch (Exception $e){
			
			//header('location:/login');
			var_dump($e);
			die();
		}
    }
	 
    public function Details($candidateId){
        try {
            
			// Authentication check
        	$this->_checkSession();
			
			$authorization = new Permission();
		    $authorization->IsAuthorized("Manage");
			 
			$candidates = $this->_model->GetCandidatesById($candidateId);
			 
            if ($candidates){
				$this->_view->set('title', "Beheertool :: Functies bewerken");
                $this->_view->set('value', $candidates);
            }
            else{
				header('location:/login');
				die();
            }
             
            return $this->_view->output();
              
        } catch (Exception $e){
            echo "Application error:" . $e->getMessage();
        }
    }
	
	public function Remove(){
        try {
            
			// Authentication check
        	$this->_checkSession();
			
			$authorization = new Permission();
		    $authorization->IsAuthorized("Manage");
        	
			//check cSRF
			$this->_checkcsrf($_POST['CSRF']);
			
            $candidate = new CandidateModel();
			$candidate->SetCandidateId($_POST['candidateId']);
			$candidate->RemoveCandidate();
            
			header('location:/candidate/');
              
        } catch (Exception $e){
            echo "Application error:" . $e->getMessage();
        }
    }
}