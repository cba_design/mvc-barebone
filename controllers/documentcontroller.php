<?php

require_once("class.permission.php");
require_once("class.uploads.php");

class DocumentController extends Controller{
		
    public function __construct($model, $action){
        parent::__construct($model, $action);
        $this->_setModel($model);
    }
    	     
	public function Edit($update){
		
		//Authentication check        	         	     
        $this->_checkSession();
			
		$authorization = new Permission();
		$authorization->IsAuthorized("Manage");
		
		//Check CSRF
		$this->_checkCsrf($_POST['CSRF']);
		
		$errors = array();
		$check = true;
		
		$documentName = isset($_POST['documentName']) ? trim($_POST['documentName']) : NULL;
		$oldname      = isset($_POST['oldname']) ? trim($_POST['oldname']) : NULL;
		$documentId   = isset($_POST['documentId']) ? trim($_POST['documentId']) : NULL;
		$description  = isset($_POST['description']) ? trim($_POST['description']) : NULL;
		$companyId    = isset($_POST['companyId']) ? trim($_POST['companyId']) : NULL;
		
		$upload = new FileUpload();
		
		if($_FILES['document']){
			$upload -> UploadDocuments($_FILES['document'], $companyId);
			$documentName = $_FILES['document']['name'];
		}
		
		//this function should be rewritten as soons as documents become private
		if($_POST['documentName']){
			$upload -> OverwriteFile($documentName, $oldname, $companyId);
		}
		
		try {
			$document = new DocumentModel();
			$document->SetDocumentName($documentName);
			$document->SetDescription($description);
			$document->SetCompanyId($companyId);
			$document->SetDocumentId($documentId);
			
			if($update == ""){
			    $document->StoreDocument();
			}
			if($update == "update"){
				$document->UpdateDocument();
			}
			if(!preg_match("/^[0-9]+$/", $companyId)){
				header('location:/login');
				die();
			}
			
			setcookie("cameFrom", "document", time() + (86400 * 30), "/");
			header('location:/company/details/'.$companyId);
			
		} catch (Exception $e){
			header('location:/login');
			die();
		}
    }
	 
    public function Details($documentId){
        try {
            
			// Authentication check
        	$this->_checkSession();
			
			$authorization = new Permission();
		    $authorization->IsAuthorized("Manage");
			 
			$documents = $this->_model->GetDocumentsById($documentId);
			 
            if ($documents){
				$this->_view->set('title', "Beheertool :: docunent bewerken");
                $this->_view->set('documents', $documents);
            }
            else{
				header('location:/login');
				die();
            }
             
            return $this->_view->output();
              
        } catch (Exception $e){
            echo "Application error:" . $e->getMessage();
        }
    }
	
	public function Download($documentId){
		
			// Authentication check
        	$this->_checkSession();
			
			$authorization = new Permission();
		    $authorization->IsAuthorized("Manage");
			
			$documents = $this->_model->GetDocumentsById($documentId);
			
			foreach($documents as $val){
				$filename = $val['Name'];
			}
			
			header('Content-Description: File Transfer');
            header('Content-Type: application/vnd.oasis.opendocument.text');
            header('Content-Disposition: attachment; filename='.$filename);
            header('Expires: 0');
            header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
            header("Cache-Control: post-check=0, pre-check=0", false);
            header("Pragma: no-cache");		
			
			echo file_get_contents('../uploads/'.$filename);

	}
	
	public function Remove(){
        try {
            
			// Authentication check
        	$this->_checkSession();
			
			$authorization = new Permission();
		    $authorization->IsAuthorized("Manage");
        	
			//Check CSRF
			$this->_checkCsrf($_POST['CSRF']);
			
			//When removing a file select it from databse in order to prevent illigal deletion of docs like .conf with 
			$documents = $this->_model->GetDocumentsById($_POST['documentId']);
			
			foreach($documents as $val){
				$filename = $val['Name'];
			}
			unlink('../uploads/'.$filename);
			
			$document = new DocumentModel();
			$document->SetDocumentId($_POST['documentId']);
			$document->RemoveDocument();
            
			if(!preg_match("/^[0-9]+$/", $_POST['companyId'])){
				header('location:/login');
				die();
			}
			header('location:/company/details/'.$_POST['companyId']);
              
        } catch (Exception $e){
            echo "Application error:" . $e->getMessage();
        }
    }
}