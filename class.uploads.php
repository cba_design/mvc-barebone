<?php

class FileUpload{
	
	public function UploadDocuments($document, $companyId){
		
		$check = true;
		
		//File location outside of the root
		$uploaddir = '../uploads/';

		$filetype = explode(".", $document['name']);
		
		$takeLastValue = count($filetype) - 1;
		
		if(($filetype[$takeLastValue] != "docx" ) && ($filetype[$takeLastValue] != "odt") && ($check == true )){
			$check = false;
			setcookie("error", "Foutief bestandstype!", time() + (86400 * 30), "/");
		}
		//For uploading out of intended directory we check the filename and verify that it only contains alpahnumeric values.
		if(!preg_match("/^[a-zA-Z0-9]+$/", $filetype[0]) && ($check == true )){
			$check = false;
			setcookie("error", "Gelieve enkel alleen alphanummerieke bestandsnamen!", time() + (86400 * 30), "/");
		}
		
		// Check if file already exists to prevent overwriting
		if((file_exists($uploaddir.$document['name']) && ($check == true ))) {
			$check = false;
			setcookie("error", "Bestandsnaam bestaat reeds al", time() + (86400 * 30), "/");
		}
		
		if($check == true){
			$uploadfile = $uploaddir . basename($document['name']);
			move_uploaded_file($document['tmp_name'], $uploadfile);
			
			//Last mime type check after upload if not correct than delete!
			$finfo = finfo_open(FILEINFO_MIME_TYPE);
			$theType = finfo_file($finfo, $uploaddir.$document['name']);

			if(($theType != "application/vnd.openxmlformats-officedocument.wordprocessingml.document") && ($theType != "application/vnd.oasis.opendocument.text") && ($check == true )){    
				unlink($uploaddir.$document['name']);
				$check = false;   
				setcookie("error", "Foutief bestandstype", time() + (86400 * 30), "/");			
			}
		}
		//prevent header injection by companyId
		if(!preg_match("/^[0-9]+$/", $companyId)){
				header('location:/login');
				die();
			}
		
		if($check == false){
			header('location:/company/details/'.$companyId);
			die();
		}
	}
}